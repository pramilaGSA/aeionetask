package com.aeion.task;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;

import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.aeion.task.databinding.ActivityMainBinding;

import java.util.ArrayList;


public class MainActivity extends FragmentActivity implements OnMapReadyCallback , GoogleMap.OnMarkerClickListener {

    GoogleMap googleMap;
    ArrayList<LatLngBean> arrayList = new ArrayList<LatLngBean>();
    BottomSheetBehavior sheetBehavior;

    ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        setData ();


        binding.qrScanner.setOnClickListener(view -> {
            startActivity(new Intent(MainActivity.this,ScannedBarcodeActivity.class));
        });
    }


    private void setData() {
        LatLngBean bean = new LatLngBean();
        bean.setLatitude(12.9525999);
        bean.setLongitude(77.5648000);
        arrayList.add(bean);

        LatLngBean bean1 = new LatLngBean();
        bean1.setLatitude(12.94360000);
        bean1.setLongitude(77.589900);
        arrayList.add(bean1);

        LatLngBean bean2 = new LatLngBean();
        bean2.setLatitude(12.9336000);
        bean2.setLongitude(77.5399000);
        arrayList.add(bean2);

        LatLngBean bean3 = new LatLngBean();
        bean3.setLatitude(12.962599);
        bean3.setLongitude(77.5140);
        arrayList.add(bean3);

        LatLngBean been4 = new LatLngBean();
        been4.setLatitude(12.9725999);
        been4.setLongitude(77.57000);
        arrayList.add(been4);

        LatLngBean been5 = new LatLngBean();
        been5.setLatitude(12.98599);
        been5.setLongitude(77.50000);
        arrayList.add(been4);

    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap = googleMap;

        for (int i = 0; i < arrayList.size (); i++) {
            LatLngBean location = arrayList.get (i);
            LatLng position = new LatLng ((location.getLatitude ()), (location.getLongitude ()));
           googleMap.addMarker (new MarkerOptions ().
                   position (position).
                   icon (BitmapDescriptorFactory.
                           fromBitmap (getTheMarker ())));
            googleMap.setOnMarkerClickListener(this);


            if (i == 0) {
                googleMap.moveCamera (CameraUpdateFactory.
                        newLatLngZoom (new LatLng (location.getLatitude (),
                        location.getLongitude ()), 12.0f));
            }
        }
    }

        private Bitmap getTheMarker() {
            int height = 150;
            int width =150;
            BitmapDrawable bitmapdraw;
            bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.gps);
            Bitmap b = bitmapdraw.getBitmap();
            Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
            return smallMarker;
        }

    @Override
    public boolean onMarkerClick(Marker marker) {
        binding.bottomSheet.setVisibility(View.VISIBLE);
        sheetBehavior = BottomSheetBehavior.from(binding.bottomSheet);
        sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        return false;
    }

}